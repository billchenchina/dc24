from csv import DictReader

from django.core.management.base import BaseCommand

from register.models import Attendee, Accomm


class Command(BaseCommand):
    help = 'Load room assignments from CSV'

    def add_arguments(self, parser):
        parser.add_argument('csv', type=open,
                            help='CSV File with room & headername columns '
                                 '(and a header)')
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something')

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')
        r = DictReader(options['csv'])
        for line in r:
            try:
                attendee = Attendee.objects.get(user__username=line['username'])
            except Attendee.DoesNotExist:
                print(f"Cannot find {line['username']}, skipping")
                continue
            try:
                accomm = attendee.accomm
            except Accomm.DoesNotExist:
                print(f"{line['username']} didn't request accommodation")
                continue
            else:
                if not dry_run:
                    accomm.room = line['room']
                    accomm.save()
